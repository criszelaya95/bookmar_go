package model

import (
	"github.com/criszelaya24/Go/db"
)

func NewUser(name string, u_name string, email string, password string) (string, string, error) {

	q := `INSERT INTO users (name, u_name, email, password) 
		VALUES ($1, $2, $3, $4)  RETURNING u_name, password`

	err := postgres.DbClient.QueryRow(q, name, u_name, email, password).Scan(&u_name, &password)
	if err != nil {
		return "", "", err
	}
	return u_name, password, nil
}

func CreateSession(username, password string) (string, error)  {

	q := `SELECT * FROM users WHERE
							u_name = $1
							AND password = $2`
	var id string
	_, err := postgres.DbClient.Query(q, username, password)
	if err != nil {
		return " ", err
	}
	return id, nil
}