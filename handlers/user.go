package handler

import "C"
import (
	"fmt"
	"github.com/criszelaya24/Go/model"
	"github.com/gin-gonic/gin"
	"net/http"
)

// index function
func Signup(c *gin.Context) {
  c.HTML(200, "signup.html", nil)
}

func SignupCreate(c *gin.Context) {
	username, password, err := model.NewUser(c.PostForm("name"), c.PostForm("username"), c.PostForm("email"), c.PostForm("password"))
	if err != nil {
		fmt.Println(err)
	}
	//model.CreateSession(username, password)
	fmt.Println(username, password)
	_, err = model.CreateSession(username, password)

	if err != nil {
		fmt.Println(err)
	}
	c.JSON(http.StatusOK, gin.H{
		"message": "Loged in!",
	})
}

func Login(c *gin.Context)  {
	id, err := model.CreateSession(c.PostForm("username"), c.PostForm("password"))

	if err != nil {
		fmt.Println(err)
	}
	c.JSON(http.StatusOK, gin.H{
		"id": id,
	})
}